package ru.ege.geo.json;

import org.codehaus.jackson.annotate.JsonIgnore;
import ru.ege.geo.util.GeoPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * @author afilatov
 */
abstract public class JSONRouteMixin {
    @JsonIgnore
    public List<GeoPoint> dots = new ArrayList<>();
}
