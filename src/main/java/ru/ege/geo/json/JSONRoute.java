package ru.ege.geo.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import ru.ege.geo.bean.MetroBean;
import ru.ege.geo.util.GeoPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * @author afilatov
 */
@JsonIgnoreProperties({"metroStation"})
public class JSONRoute {

    public MetroBean metroStation;

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    public Integer stationId;
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    public String stationTitle;

    public double distance = 0;

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    public Integer travelTime;

    public List<GeoPoint> dots = new ArrayList<>();

    public JSONRoute(MetroBean metroStation) {
        if(metroStation != null) {
            this.metroStation = metroStation;
            this.stationId = metroStation.getId();
            this.stationTitle = metroStation.getTitle();
        }
    }

    /*
        Время в пути, в секундах
    */
    public Integer getTravelTime() {
        int velocity = distance > 1.2 ? 10 : 5;
        return (int) (distance * 3600 / velocity);
    }

}
