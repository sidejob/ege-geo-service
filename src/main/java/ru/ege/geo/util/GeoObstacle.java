package ru.ege.geo.util;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author afilatov
 */
public class GeoObstacle {
    private int id;
    private List<GeoPoint> points = new ArrayList<>();
    private Map<GeoPoint, Integer> pointsMap = new HashMap<>();
    private List<GeoObstacleSeg> segments = new ArrayList<>();
    private ObstacleType type;

    private GeoObstacle(ObstacleType type, int id) {
        this.type = type;
        this.id = id;
    }

    public List<GeoObstacleSeg> getSegments() {
        return segments;
    }

    public List<GeoPoint> getPoints() {
        return points;
    }

    public GeoPoint getFirstPoint() {
        return points.get(0);
    }

    public GeoPoint getLastPoint() {
        return points.get(points.size() - 1);
    }

    public int getPointIndex(GeoPoint geoPoint) {
        return pointsMap.get(geoPoint);
    }

    public ObstacleType getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public GeoPoint getPoint(int idx) {
        return points.get(idx);
    }

    private int getSegmentCount() {
        if (points.size() <= 1) {
            return 0;
        }

        switch (type) {
            case LINE:
                return points.size() - 1;
            case RING:
                if (points.size() <= 2)
                    return 0;
                else
                    return points.size();
            default:
                throw new IllegalArgumentException("Unsupported Obstacle type");
        }
    }

    private GeoObstacleSeg makeSegment(int i) {
        if (points.size() <= 1) {
            return null;
        }
        if (i == 0) {
            return null;
        }

        switch (type) {
            case LINE:
                return new GeoObstacleSeg(points.get(i - 1), points.get(i));
            case RING:
                if (points.size() <= 2)
                    return null;
                else if (i == points.size()) {
                    return new GeoObstacleSeg(points.get(points.size() - 1), points.get(0));
                } else {
                    return new GeoObstacleSeg(points.get(i - 1), points.get(i));
                }
            default:
                return null;
        }
    }

    public static GeoObstacle parseString(String s, ObstacleType type, int id) {
        GeoObstacle result = new GeoObstacle(type, id);

        String[] coords = StringUtils.split(s, " ");
        for (String latLng : coords) {
            GeoPoint geoPoint = GeoPoint.parseString(latLng);
            geoPoint.setObstacle(result);
            if (!result.pointsMap.containsKey(geoPoint)) {
                result.points.add(geoPoint);
                result.pointsMap.put(geoPoint, result.points.size() - 1);
            }
        }

        for (int i = 1; i <= result.getSegmentCount(); i++) {
            GeoObstacleSeg segment = result.makeSegment(i);
            result.segments.add(segment);
        }
        return result;
    }

    public DirectionWay defineDirection(GeoPoint point, GeoObstacleSeg segment) {

        int i1 = pointsMap.get(point);

        int i2 = pointsMap.get(segment.from);
        int i3 = pointsMap.get(segment.to);

        if (i1 <= i2 && i1 <= i3) {
            return DirectionWay.DOWN;
        }

        if (i1 >= i2 && i1 >= i3) {
            return DirectionWay.UP;
        }

        throw new RuntimeException("Error defining direction");
    }

    public GeoPoint getNextPoint(GeoPoint geoPoint, DirectionWay direction) {

        int index = pointsMap.get(geoPoint);

        if (DirectionWay.UP.equals(direction)) {
            index = index + 1;
            if (index > points.size() - 1) {
                switch (type) {
                    case LINE:
                        return null;
                    default:
                        index = 0;
                }
            }
        }

        if (DirectionWay.DOWN.equals(direction)) {
            index = index - 1;
            if (index < 0) {
                switch (type) {
                    case LINE:
                        return null;
                    default:
                        index = points.size() - 1;
                }
            }
        }
        return points.get(index);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoObstacle obstacle = (GeoObstacle) o;

        if (id != obstacle.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

}

