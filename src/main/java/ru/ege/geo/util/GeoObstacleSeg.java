package ru.ege.geo.util;

/**
 * @author afilatov
 */
public class GeoObstacleSeg {

    final public GeoPoint from;
    final public GeoPoint to;

    public GeoObstacleSeg(GeoPoint from, GeoPoint to) {
        this.from = from;
        this.to = to;
    }

    public boolean containsPoint(GeoPoint point) {
        if(point == null) return false;
        return point.equals(from) || point.equals(to);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoObstacleSeg that = (GeoObstacleSeg) o;

        if (!from.equals(that.from)) return false;
        if (!to.equals(that.to)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = from.hashCode();
        result = 31 * result + to.hashCode();
        return result;
    }
}
