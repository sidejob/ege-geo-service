package ru.ege.geo.util;

import com.bbn.openmap.geo.Geo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ege.geo.bean.MetroBean;
import ru.ege.geo.bean.ObstacleStorageBean;
import ru.ege.geo.json.JSONRoute;
import ru.ege.geo.json.JSONRouteList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;

/**
 * @author afilatov
 */
public class RouteBuilder implements Callable<JSONRouteList> {
    private static final Logger LOGGER = LoggerFactory.getLogger(RouteBuilder.class);

    public static enum Filter {
        ALL(999),
        SHORTEST_1(1),
        SHORTEST_2(2),
        SHORTEST_3(3),
        SHORTEST_4(4);

        Filter(int count) {
            this.count = count;
        }

        private int count = 0;
    }

    public static enum Spread {
        NOCHOP,
        CHOP
    }

    private Filter filter;
    private Spread spread;

    private ObstacleStorageBean obstaclesBean;

    private MetroBean destinationStation;
    private GeoPoint destinationPoint;
    private TreeNode<GeoNode> rootNode;

    private Map<GeoObstacle, Integer> obstaclePassed = new HashMap<>();
    private List<TreeNode<GeoNode>> leafs = new LinkedList<>();
    private int recoursiveDepth = 0;

    private JSONRouteList futureResult = new JSONRouteList();

    public RouteBuilder(Filter filter, Spread spread,
                        ObstacleStorageBean obstaclesBean, GeoPoint startPoint, GeoPoint destinationPoint) {
        this.filter = filter;
        this.spread = spread;
        this.obstaclesBean = obstaclesBean;
        recoursiveDepth = 0;
        this.destinationPoint = destinationPoint;
        rootNode = new TreeNode<>(new GeoNode(startPoint));
        leafs.clear();
        obstaclePassed.clear();
    }

    public RouteBuilder(Filter filter, Spread spread,
                        ObstacleStorageBean obstaclesBean, GeoPoint startPoint, MetroBean destinationStation) {
        this(filter, spread, obstaclesBean, startPoint, new GeoPoint(destinationStation.getLat(), destinationStation.getLng()));
        this.destinationStation = destinationStation;
    }

    /**
     * Возможные улучшения:
     * 1. Обработка ситуации "точка внутри препятствия"
     * 2. Срезать углы до нахождения оптимального маршрута, а не после
     * 3. Лучше обрабатывать ситуацию когда препятствия пересекают друг друга
     */
    @Override
    public JSONRouteList call() throws Exception {

//        if(obstaclesBean.isPointInObstacle(rootNode.data.point)) {
//            LOGGER.info("Point inside obstacle");
//        }

        build(rootNode, null, null, null);

        if (leafs.isEmpty()) {

            if (obstaclePassed.size() == 1 && ObstacleType.RING.equals(obstaclePassed.keySet().iterator().next().getType())) {
                // отправная точка лежит внутри препятствия
                leafs.add(rootNode.addChild(new GeoNode(destinationPoint)));
            } else {
                LOGGER.info("Unable to build route: from, to");
                LOGGER.info(rootNode.data.point.js());
                LOGGER.info(destinationPoint.js());
                return futureResult;
            }
        }

/*
        for (TreeNode<GeoNode> leaf : leafs) {
            int i = 1;
            while(i > 0) {
                i = chopShortcuts(leaf, 0);
            }
        }
*/

        // определяем расстояние маршрута
        TreeMap<Double, TreeNode<GeoNode>> routeDistance = new TreeMap<>();
        for (TreeNode<GeoNode> leaf : leafs) {
            double distance = calculateRouteDistance(leaf);
            routeDistance.put(distance, leaf);
        }

        int routeCount = 0;

        for (Map.Entry<Double, TreeNode<GeoNode>> entry : routeDistance.entrySet()) {
            routeCount++;
            if(routeCount > filter.count) {
                break;
            }

            LinkedList<TreeNode<GeoNode>> routeNodes = new LinkedList<>();
            walkPath(entry.getValue(), routeNodes);

            if (Spread.CHOP.equals(spread)) {
                int i = 1;
                while (i > 0) {
                    i = chopShortcuts(routeNodes);
                }

/*
            int i = 1;
            while (i > 0) {
                i = chopShortcuts(routeDistance.get(routeDistance.firstKey()), 0);
            }
*/
            }

/*
        for (TreeNode<GeoNode> leaf : leafs) {
            JSONRoute jsonRoute = new JSONRoute(destinationStation);
            jsonRouteList.routes.add(jsonRoute);
            walkPath(leaf, jsonRoute);
        }
*/
            JSONRoute jsonRoute = new JSONRoute(destinationStation);
            jsonRoute.distance = entry.getKey();
            futureResult.routes.add(jsonRoute);
            walkPath(routeNodes, jsonRoute);
            // walkPath(routeDistance.get(routeDistance.firstKey()), jsonRoute);

        }
        return futureResult;
    }

    private int chopShortcuts(LinkedList<TreeNode<GeoNode>> routeList) {
        int result = 0;
        for (int i = 0; i < routeList.size(); i++) {
            if (i + 2 < routeList.size()) {
                TreeNode<GeoNode> currentNode = routeList.get(i);
                TreeNode<GeoNode> nexNode = routeList.get(i + 2);

                GeoObstacle middleObstacle = routeList.get(i + 1).data.obstacle;
                GeoPoint middlePoint = routeList.get(i + 1).data.point;

                boolean cutWay = true;

                if (ObstacleType.LINE.equals(middleObstacle.getType()) && (
                        middleObstacle.getFirstPoint().equals(middlePoint) || middleObstacle.getFirstPoint().equals(middlePoint))) {
                    cutWay = false;
                }

                if (currentNode.data.point.equals(nexNode.data.point)) {
                    cutWay = false;
                }

                if (cutWay && !checkBearing(currentNode, nexNode.data.point)) {
                    cutWay = false;
                }
//                if (cutWay && !checkBearing(nexNode, currentNode.data.point)) {
//                    cutWay = false;
//                }

                if (cutWay && obstaclesBean.hasIntersectionsWithSelfExeptions(currentNode.data.point, nexNode.data.point)) {
                    cutWay = false;
                }

                if (cutWay) {
                    routeList.remove(i + 1);
                    result++;
                }
            }
        }

        return result;
    }

    private int chopShortcuts(TreeNode<GeoNode> leaf, int i) {
        int result = i;

        if (leaf.parent != null && leaf.parent.parent != null) {
            boolean cutWay = true;
            TreeNode<GeoNode> secondParent = leaf.parent.parent;

            if (!checkBearing(secondParent, leaf.data.point)) {
                cutWay = false;
            }
            if (cutWay && !checkBearing(leaf, secondParent.data.point)) {
                cutWay = false;
            }
            if (cutWay && obstaclesBean.hasIntersectionsWithSelfExeptions(leaf.data.point, secondParent.data.point)) {
                cutWay = false;
            }

            if (cutWay) {
                leaf.parent = secondParent;
                result += chopShortcuts(leaf, result) + 1;

            } else {
                result += chopShortcuts(leaf.parent, result);
            }

        }

        return result;
    }


    private void walkPath(TreeNode<GeoNode> leaf, JSONRoute route) {
        route.dots.add(leaf.data.point);
        if (!leaf.isRoot()) {
            walkPath(leaf.parent, route);
        }
    }

    private void walkPath(LinkedList<TreeNode<GeoNode>> routeList, JSONRoute jsonRoute) {
        for (TreeNode<GeoNode> node : routeList) {
            jsonRoute.dots.add(node.data.point);
        }
    }


    private void walkPath(TreeNode<GeoNode> leaf, LinkedList<TreeNode<GeoNode>> routeList) {
        routeList.addFirst(leaf);
        if (!leaf.isRoot()) {
            walkPath(leaf.parent, routeList);
        }
    }

    private double calculateRouteDistance(TreeNode<GeoNode> leaf) {
        double result = 0;
        if (!leaf.isRoot()) {
            Geo point1 = new Geo(leaf.data.point.getLat(), leaf.data.point.getLng());
            Geo point2 = new Geo(leaf.parent.data.point.getLat(), leaf.parent.data.point.getLng());
            result = result + point1.distanceKM(point2);
            result = result + calculateRouteDistance(leaf.parent);
        }
        return result;
    }


    public boolean build(TreeNode<GeoNode> node, GeoPoint candidatePoint, GeoObstacle candidateObstacle, LinkedList<GeoPoint> queuePoints) {
        recoursiveDepth++;
        //LOGGER.info("recoursiveDepth = " + recoursiveDepth);
        if (recoursiveDepth > 500) {
            recoursiveDepth = 0;
            return false;
        }

        //LOGGER.info("node.getLevel() = " + node.getLevel());
        if (node.getLevel() > 25)
            return false;

        // проверяем угол, если он есть
        boolean bearingCheck = checkBearing(node, candidatePoint != null ? candidatePoint : destinationPoint);
        if (!bearingCheck) {
            // ползём по краю
            walkOnBorder(node, queuePoints);
            return false;
        }
        // пытаемся проложить путь до пункта назначения или обходной точки
        GeoCrossing crossing = obstaclesBean.findClosestIntersection(node.data.point, candidatePoint != null ? candidatePoint : destinationPoint);

        if (crossing != null) {
            // есть препятствие
            if (crossing.obstacle.equals(candidateObstacle)) {
                // наткнулись на себя 2ой раз
                return false;
            } else if (crossing.obstacle.equals(node.data.obstacle)) {
                // наткнулись на себя 2ой раз, но уже стоим на краю этого препятствия
                // ЛИБО ЗАККОМЕНТИТЬ, ЛИБО ПРИДУМАТЬ ПРОВЕРКУ ЧТО ТОЧКИ КУДА ПОЛЗЁМ ЛЕЖАТ НА ТОЙ ЖЕ ФИГУРЕ ЧТО И NODE !!!
                // СМ. ТЕТРАДЬ 1
                walkOnBorder(node, queuePoints);

            } else {
                // новая преграда
                // предложить новые точки обхода

                // удостоверимся, что действительно новая преграда
                // защита от зацикливания
                Integer passedCount = obstaclePassed.get(crossing.obstacle);
                obstaclePassed.put(crossing.obstacle, passedCount == null ? 1: passedCount + 1);
                if (obstaclePassed.get(crossing.obstacle) < 3) { // эмпирически, не больее 3х пересечений с самим собой
                    proposeCandidatePoints(node, crossing);
                }
            }

        } else {
            if (candidatePoint != null) {
                // ставим точку только если до неё нет препятствий
                // первая точка на препятствии

                TreeNode<GeoNode> newNode = node.addChild(new GeoNode(candidatePoint));
                newNode.data.obstacle = candidateObstacle;
                // вычислить угол и заполнить
                calculateBearings(node, candidatePoint, candidateObstacle, newNode);

                return build(newNode, null, null, queuePoints);
            } else {
                // конечная точка
                TreeNode<GeoNode> finishNode = node.addChild(new GeoNode(destinationPoint));
                leafs.add(finishNode);
                return true;
            }
        }
        return false;
    }

    private void proposeCandidatePoints(TreeNode<GeoNode> node, GeoCrossing crossing) {
        for (GeoPoint geoPoint : new GeoPoint[]{crossing.segment.from, crossing.segment.to}) {
            // for (GeoPoint geoPoint : crossing.obstacle.getPoints()) {

            GeoObstacleSeg crossingSegment = crossing.segment;
            GeoPoint nextPoint;
            LinkedList<GeoPoint> queue1 = new LinkedList<>();
            LinkedList<GeoPoint> queue2 = new LinkedList<>();

            nextPoint = crossing.obstacle.getNextPoint(geoPoint, DirectionWay.UP);
            while (nextPoint != null && !nextPoint.equals(crossingSegment.from) && !nextPoint.equals(crossingSegment.to)) {
                queue1.add(nextPoint);
                nextPoint = crossing.obstacle.getNextPoint(nextPoint, DirectionWay.UP);
            }
            nextPoint = crossing.obstacle.getNextPoint(geoPoint, DirectionWay.DOWN);
            while (nextPoint != null && !nextPoint.equals(crossingSegment.from) && !nextPoint.equals(crossingSegment.to)) {
                queue2.add(nextPoint);
                nextPoint = crossing.obstacle.getNextPoint(nextPoint, DirectionWay.DOWN);
            }

            if (!queue1.isEmpty() && ObstacleType.LINE.equals(crossing.obstacle.getType())) {
                // дополнить обход в обратную сторону
                int pointIdx = crossing.obstacle.getPointIndex(queue1.getLast());
                List<GeoPoint> tmp = new ArrayList<>(crossing.obstacle.getPoints());
                if (pointIdx != 0)
                    Collections.reverse(tmp);
                queue1.addAll(tmp);
            }

            if (!queue2.isEmpty() && ObstacleType.LINE.equals(crossing.obstacle.getType())) {
                // дополнить обход в обратную сторону
                int pointIdx = crossing.obstacle.getPointIndex(queue2.getLast());
                List<GeoPoint> tmp = new ArrayList<>(crossing.obstacle.getPoints());
                if (pointIdx != 0)
                    Collections.reverse(tmp);
                queue2.addAll(tmp);
            }


            // указать направление дальнейшего движения
            build(node, geoPoint, crossing.obstacle, queue1);
            build(node, geoPoint, crossing.obstacle, queue2);
        }
    }

    private void walkOnBorder(TreeNode<GeoNode> node, LinkedList<GeoPoint> queuePoints) {
        if (queuePoints != null && !queuePoints.isEmpty()) {
            GeoPoint nextPoint = queuePoints.removeFirst();
            // ползём только если следующая точка лежит на этой же фигуре
            if (!node.data.obstacle.equals(nextPoint.getObstacle()))
                return;

            // можем ли мы пересеч другую фигуру идя по краю ??? хз, пока нет проверки
            GeoCrossing crossing = obstaclesBean.findClosestIntersection(node.data.point, nextPoint);
            if (crossing != null)
                return;
            // ставим точку
            TreeNode<GeoNode> newNode = node.addChild(new GeoNode(nextPoint));
            newNode.data.obstacle = node.data.obstacle;
            // вычисляем угол
            calculateBearings(node, nextPoint, node.data.obstacle, newNode);

            build(newNode, null, null, queuePoints);
        }
    }

    private boolean checkBearing(TreeNode<GeoNode> node, GeoPoint candidatePoint) {

        boolean result = true;

        if (node.data.bearing != null) {
            double bearing = GeoUtil.bearing(node.data.point, candidatePoint);

            double bearingMin = Math.min(node.data.bearing.startAngle, node.data.bearing.endAngle);
            double bearingMax = Math.max(node.data.bearing.startAngle, node.data.bearing.endAngle);

            boolean bearingCheck = bearing > bearingMin && bearing < bearingMax;

            if (node.data.bearing.startAngle == bearingMin) {
                result = bearingCheck;
            } else {
                result = !bearingCheck;
            }
        }
        return result;
    }

    private void calculateBearings(TreeNode<GeoNode> node, GeoPoint candidatePoint, GeoObstacle candidateObstacle, TreeNode<GeoNode> newNode) {
        double bearing = GeoUtil.bearing(candidatePoint, node.data.point);

        GeoPoint bearingPoint1 = candidateObstacle.getNextPoint(candidatePoint, DirectionWay.UP);
        GeoPoint bearingPoint2 = candidateObstacle.getNextPoint(candidatePoint, DirectionWay.DOWN);

        if (bearingPoint1 == null || bearingPoint2 == null) {
            // наше препятствие - прямая, и candidatePoint - его конец/начало
            return;
        }

        double bearing1 = GeoUtil.bearing(candidatePoint, bearingPoint1);
        double bearing2 = GeoUtil.bearing(candidatePoint, bearingPoint2);

/*
        double bearingMin = Math.min(bearing1, bearing2);
        double bearingMax = Math.max(bearing1, bearing2);
        if(bearing > bearingMin && bearingMin < bearingMax) {
            // bearingMin > bearingMax
        } else {
            // bearingMax > bearingMin
        }
*/
//            if (node.data.bearing != null) {
        if (candidateObstacle.equals(node.data.obstacle)) {
            // ползём по краю

            // обработка случая если мы ползём от конца отрезка где нету угла
            if (node.data.bearing == null)
                return;

            GeoPoint adjacentPoint = null;
            double adjacentAngle = 0;
            if (bearingPoint1.equals(node.data.point)) {
                adjacentPoint = bearingPoint1;
                adjacentAngle = bearing1;
            } else if (bearingPoint2.equals(node.data.point)) {
                adjacentPoint = bearingPoint2;
                adjacentAngle = bearing2;
            }

            if (adjacentPoint == null) {
                LOGGER.info("adjacentPoint == null");
            }

            newNode.data.bearing = new GeoNode.Bearing();
            if (node.data.bearing.startPoint.equals(candidatePoint)) {
                newNode.data.bearing.endAngle = adjacentAngle;
                newNode.data.bearing.endPoint = adjacentPoint;
                newNode.data.bearing.startAngle = adjacentPoint.equals(bearingPoint1) ? bearing2 : bearing1;
                newNode.data.bearing.startPoint = adjacentPoint.equals(bearingPoint1) ? bearingPoint2 : bearingPoint1;
            } else if (node.data.bearing.endPoint.equals(candidatePoint)) {
                newNode.data.bearing.startAngle = adjacentAngle;
                newNode.data.bearing.startPoint = adjacentPoint;
                newNode.data.bearing.endAngle = adjacentPoint.equals(bearingPoint1) ? bearing2 : bearing1;
                newNode.data.bearing.endPoint = adjacentPoint.equals(bearingPoint1) ? bearingPoint2 : bearingPoint1;
            }

            if (newNode.data.bearing.startAngle == 0.0 || newNode.data.bearing.endAngle == 0.0) {
                LOGGER.info("newNode.data.bearing.startAngle == 0.0 || newNode.data.bearing.endAngle == 0.0");
            }


        } else {
            double bearingEnd;
            ArrayList<Double> bearings = new ArrayList<>(2);
            // [ bearing >> 360 ]
            if (bearing1 > bearing && bearing1 <= 360) {
                bearings.add(bearing1);
            }
            if (bearing2 > bearing && bearing2 <= 360) {
                bearings.add(bearing2);
            }
            if (bearings.size() == 1) {
                bearingEnd = bearings.get(0);
            } else {
                bearingEnd = Math.min(bearing1, bearing2);
            }

            newNode.data.bearing = new GeoNode.Bearing();
            if (bearingEnd == bearing1) {
                newNode.data.bearing.endAngle = bearing1;
                newNode.data.bearing.endPoint = bearingPoint1;
                newNode.data.bearing.startAngle = bearing2;
                newNode.data.bearing.startPoint = bearingPoint2;
            } else {
                newNode.data.bearing.endAngle = bearing2;
                newNode.data.bearing.endPoint = bearingPoint2;
                newNode.data.bearing.startAngle = bearing1;
                newNode.data.bearing.startPoint = bearingPoint1;
            }

            if (newNode.data.bearing.startAngle == 0.0 || newNode.data.bearing.endAngle == 0.0) {
                LOGGER.info("newNode.data.bearing.startAngle == 0.0 || newNode.data.bearing.endAngle == 0.0");
            }

        }
    }
}
