package ru.ege.geo.util;

/**
 * @author afilatov
 */
public class GeoUtil {

    public static double bearing(double latA, double lngA, double latB, double lngB) {
        double longDiff = lngB - lngA;
        double y = Math.sin(longDiff) * Math.cos(latB);
        double x = Math.cos(latA) * Math.sin(latB) - Math.sin(latA) * Math.cos(latB) * Math.cos(longDiff);
        return (Math.toDegrees(Math.atan2(y, x)) + 360) % 360;
    }

    public static double bearing(GeoPoint pointA, GeoPoint pointB) {
        double longDiff = pointB.getLng() - pointA.getLng();
        double y = Math.sin(longDiff) * Math.cos(pointB.getLat());
        double x = Math.cos(pointA.getLat()) * Math.sin(pointB.getLat()) - Math.sin(pointA.getLat()) * Math.cos(pointB.getLat()) * Math.cos(longDiff);
        return (Math.toDegrees(Math.atan2(y, x)) + 360) % 360;
    }

}
