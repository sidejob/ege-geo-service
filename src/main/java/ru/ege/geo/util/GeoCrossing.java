package ru.ege.geo.util;

/**
 * @author afilatov
 */
public class GeoCrossing {

    final public GeoObstacle obstacle;
    final public GeoObstacleSeg segment;
    final public GeoPoint geoPoint;

    public GeoCrossing(GeoObstacle obstacle, GeoObstacleSeg segment, GeoPoint geoPoint) {
        this.obstacle = obstacle;
        this.segment = segment;
        this.geoPoint = geoPoint;
    }
}
