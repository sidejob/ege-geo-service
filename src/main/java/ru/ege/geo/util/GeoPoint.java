package ru.ege.geo.util;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author afilatov
 */

@JsonIgnoreProperties({"obstacle"})
public class GeoPoint {
    private double lat;
    private double lng;
    private GeoObstacle obstacle;

    private GeoPoint() {
    }

    public GeoPoint(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setObstacle(GeoObstacle obstacle) {
        this.obstacle = obstacle;
    }

    public GeoObstacle getObstacle() {
        return obstacle;
    }

    public static GeoPoint parseString(String s) {
        GeoPoint result = new GeoPoint();

        String[] latLng = StringUtils.split(s, ",");

        result.setLat(Double.parseDouble(latLng[1]));
        result.setLng(Double.parseDouble(latLng[0]));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoPoint geoPoint = (GeoPoint) o;

        if (Double.compare(geoPoint.lat, lat) != 0) return false;
        if (Double.compare(geoPoint.lng, lng) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(lat);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lng);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "lat:" + lat +
                ", lng:" + lng +
                '}';
    }

    public String js() {
        return "map.geoObjects.add(new ymaps.Placemark([" + lat + ", " + lng + "], {}, {preset: \"islands#redStretchyIcon\"}));";
    }
}
