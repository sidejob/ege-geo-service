package ru.ege.geo.util;

/**
 * @author afilatov
 */
public class GeoNode {
    public GeoPoint point;
    public GeoObstacle obstacle;
    public GeoObstacleSeg segment;
    public Bearing bearing;

    public GeoNode(GeoPoint point) {
        this.point = point;
    }

    public static class Bearing {
        public double startAngle;
        public double endAngle;

        public GeoPoint startPoint;
        public GeoPoint endPoint;
    }
}
