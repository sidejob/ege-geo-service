package ru.ege.geo.util;

/**
 * @author afilatov
 */
public enum DirectionWay {
    UP,
    DOWN
}
