package ru.ege.geo.util;

/**
 * @author afilatov
 */
public enum ObstacleType {
    LINE,
    RING
}
