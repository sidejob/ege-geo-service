package ru.ege.geo.processor;

import com.bbn.openmap.geo.Geo;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ege.geo.bean.ObstacleStorageBean;
import ru.ege.geo.util.GeoPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 * @author afilatov
 */
public class AngleProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AngleProcessor.class);

    private ObstacleStorageBean obstacleStorageBean;

    public ObstacleStorageBean getObstacleStorageBean() {
        return obstacleStorageBean;
    }

    public void setObstacleStorageBean(ObstacleStorageBean obstacleStorageBean) {
        this.obstacleStorageBean = obstacleStorageBean;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        HttpServletRequest request = exchange.getIn().getBody(HttpServletRequest.class);
        HttpServletResponse response = (HttpServletResponse) exchange.getIn().getHeader(Exchange.HTTP_SERVLET_RESPONSE);
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        response.setContentType("text/javascript");


        /*{ // POST 1
            StringBuffer jb = new StringBuffer();
            String line = null;
            try {
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null)
                    jb.append(line);
                LOGGER.info("POST body - " + jb);
            } catch (Exception e) {
            }
        }
        { // POST 2
            StringBuilder stringBuilder = new StringBuilder(1000);
            Scanner scanner = new Scanner(request.getInputStream());
            while (scanner.hasNextLine()) {
                stringBuilder.append(scanner.nextLine());
            }

            String body = stringBuilder.toString();
            LOGGER.info("POST body - " + body);
        }    */


        double latA = 0, lngA = 0, latB = 0, lngB = 0, latC = 0, lngC = 0, latD = 0, lngD = 0;

        Enumeration enParams = request.getParameterNames();
        while (enParams.hasMoreElements()) {
            try {
                String paramName = (String) enParams.nextElement();
                switch (paramName) {
                    case "latA":
                        latA = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "lngA":
                        lngA = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "latB":
                        latB = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "lngB":
                        lngB = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "latC":
                        latC = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "lngC":
                        lngC = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "latD":
                        latD = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "lngD":
                        lngD = Double.parseDouble(request.getParameter(paramName));
                        break;
                }
            } catch (NumberFormatException e) {
            }
        }

        {
            // поиск пересечений kml и линии
            List<GeoPoint> geoPoints = obstacleStorageBean.findIntersections(new GeoPoint(latA, lngA), new GeoPoint(latB, lngB));
            exchange.getOut().setBody(Arrays.toString(geoPoints.toArray()));
        }

//        List<GeoPoint> geoPoints = obstacleStorageBean.findMinRoute(new GeoPoint(latA, lngA), new GeoPoint(latB, lngB));
//        exchange.getOut().setBody(Arrays.toString(geoPoints.toArray()));


/*
        http://localhost:8282/ege/geo/angle?latA=55.6621504280389&lngA=37.520184343127525&latB=55.662393019662844&lngB=37.539582078723214&latC=55.67170739576139&lngC=37.53966790941171
*/

        Geo geo1 = new Geo(latA, lngA);
        Geo geo2 = new Geo(latB, lngB);
        Geo geo3 = new Geo(latC, lngC);

        /*double angleDegree = Geo.angle(geo1, geo2, geo3);

        double rad = Geo.radians(angleDegree);

        double simpleAngle1 = Geo.simpleAngle(geo1, geo2);
        double simpleAngle2 = Geo.simpleAngle(geo2, geo3);
        double simpleAngle3 = Geo.simpleAngle(geo3, geo1);

        double azimuth1 = geo1.azimuth(geo2);
        double azimuth2 = geo2.azimuth(geo3);
        double azimuth3 = geo1.azimuth(geo3);

        double sazimuth1 = geo1.strictAzimuth(geo2);
        double sazimuth2 = geo2.strictAzimuth(geo3);
        double sazimuth3 = geo1.strictAzimuth(geo3);

        exchange.getOut().setBody("angleDegree = " + angleDegree +
                        ", rad = " + rad +
                        ", simpleAngle1 = " + (simpleAngle1 * 180 / 3.14) +
                        ", simpleAngle2 = " + (simpleAngle2 * 180 / 3.14) +
                        ", simpleAngle3 = " + (simpleAngle3 * 180 / 3.14) +
                        ", azimuth1 = " + azimuth1 +
                        ", azimuth2 = " + azimuth2 +
                        ", azimuth3 = " + azimuth3 +
                        ", sazimuth1 = " + sazimuth1 +
                        ", sazimuth2 = " + sazimuth2 +
                        ", sazimuth3 = " + sazimuth3
        );                                              */

        exchange.getOut().setBody(
                "1>2 = " + bearing(latA, lngA, latB, lngB)
                        + "2>1 = " + bearing(latB, lngB, latA, lngA)
                        + ", 2>3 = " + bearing(latB, lngB, latC, lngC)
                        + ", 3>2 = " + bearing(latC, lngC, latB, lngB)
                        + ", 1>3 = " + bearing(latA, lngA, latC, lngC)
                        + ", 3>1 = " + bearing(latC, lngC, latA, lngA)
        );

    }

    public double GetDirection(double lat1, double lon1, double lat2, double lon2) {

        double br = Math.atan2(
                Math.sin(lon2 - lon1) * Math.cos(lat2),
                Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)
        ) % (2 * Math.PI);

        br = (br * 180) / Math.PI;
        return br;
    }

    protected static double bearing(double lat1, double lon1, double lat2, double lon2) {
        double longDiff = lon2 - lon1;
        double y = Math.sin(longDiff) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(longDiff);
        return (Math.toDegrees(Math.atan2(y, x)) + 360) % 360;
    }
}
