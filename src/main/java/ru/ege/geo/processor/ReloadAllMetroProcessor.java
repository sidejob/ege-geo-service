package ru.ege.geo.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ege.geo.bean.AppStatusBean;
import ru.ege.geo.bean.MetroPathStorageBean;
import ru.ege.geo.bean.MetroStorageBean;
import ru.ege.geo.bean.ObstacleStorageBean;
import ru.ege.geo.dao.GeoDAO;
import ru.ege.geo.json.JSONRoute;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author afilatov
 */
public class ReloadAllMetroProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReloadAllMetroProcessor.class);

    public static final int MINUTES_25 = 1500;
    public static final double DISTANCE_SPEED_FACTOR = 1.2;

    private ObstacleStorageBean obstaclesBean;
    private MetroStorageBean metroBean;
    private MetroPathStorageBean metroPathBean;

    private DataSource dataSource;

    public ObstacleStorageBean getObstaclesBean() {
        return obstaclesBean;
    }

    public void setObstaclesBean(ObstacleStorageBean obstaclesBean) {
        this.obstaclesBean = obstaclesBean;
    }

    public MetroStorageBean getMetroBean() {
        return metroBean;
    }

    public void setMetroBean(MetroStorageBean metroBean) {
        this.metroBean = metroBean;
    }

    public MetroPathStorageBean getMetroPathBean() {
        return metroPathBean;
    }

    public void setMetroPathBean(MetroPathStorageBean metroPathBean) {
        this.metroPathBean = metroPathBean;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        HttpServletRequest request = exchange.getIn().getBody(HttpServletRequest.class);

        AppStatusBean appStatusBean = AppStatusBean.getInstance();
        if (appStatusBean.isBusy()) {
            exchange.getOut().setBody("{\"busy\":1}");
            return;
        } else {
            appStatusBean.setBusy(true);
            appStatusBean.reset(0);
        }

        Connection connection = null;
        PreparedStatement statementSelect = null;
        PreparedStatement statementDelete = null;
        PreparedStatement statementInsert = null;
        ResultSet rs = null;
        try {
            connection = dataSource.getConnection();

            String query = "SELECT id, lat, lng FROM geo WHERE entity_type = 'client' " /*+
                "AND NOT EXISTS (SELECT 1 FROM gapi_metro WHERE gapi_metro.point_id = geo.id AND distance IS NOT NULL )"*/;

            /*JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            List<GeoDAO> geos = jdbcTemplate.query(query, new RowMapper<GeoDAO>() {
                @Override
                public GeoDAO mapRow(ResultSet rs, int rowNum) throws SQLException {
                    GeoDAO geo = new GeoDAO();
                    geo.setId(rs.getInt("id"));
                    geo.setLat(rs.getFloat("lat"));
                    geo.setLng(rs.getFloat("lng"));
                    return geo;
                }
            });*/

            statementSelect = connection.prepareStatement(query);
            rs = statementSelect.executeQuery();
            List<GeoDAO> geos = new LinkedList<>();
            while (rs.next()) {
                GeoDAO geo = new GeoDAO();
                geo.setId(rs.getInt("id"));
                geo.setLat(rs.getFloat("lat"));
                geo.setLng(rs.getFloat("lng"));
                geos.add(geo);

            }
            appStatusBean.reset(geos.size());

            ExecutorService pool = Executors.newFixedThreadPool(10);

            for (GeoDAO geoDAO : geos) {
                double latA = geoDAO.getLat(), lngA = geoDAO.getLng();

                TreeMap<Integer, JSONRoute> finalRoutes = ProximateMetroProcessor.buildRoutes(pool, latA, lngA,
                        obstaclesBean, metroBean, metroPathBean);

                //JdbcTemplate jdbcTemplateInner = new JdbcTemplate(dataSource);
                //jdbcTemplateInner.update("DELETE FROM gapi_metro WHERE point_id = ?", geoDAO.getId());

                statementDelete = connection.prepareStatement("DELETE FROM gapi_metro WHERE point_id = ?");
                statementDelete.setInt(1, geoDAO.getId());
                statementDelete.executeUpdate();
                statementDelete.close();

                for (JSONRoute route : finalRoutes.values()) {

                    statementInsert = connection.prepareStatement("INSERT INTO gapi_metro(point_id, station_id, " +
                            "r1_total, r1_current, r1_usesubway, useful, distance) VALUES(?, ?, ?, ?, ?, ?, ?)");
                    statementInsert.setInt(1, geoDAO.getId());
                    statementInsert.setInt(2, route.stationId);
                    statementInsert.setInt(3, route.travelTime / 60);
                    statementInsert.setInt(4, route.travelTime / 60);
                    statementInsert.setInt(5, 0);
                    statementInsert.setInt(6, 1);
                    statementInsert.setFloat(7, (float) route.distance);
                    statementInsert.executeUpdate();
                    statementInsert.close();
                    //jdbcTemplateInner.update("INSERT INTO gapi_metro(point_id, station_id, r1_total, distance) VALUES(?, ?, ?, ?)",
                    //geoDAO.getId(), route.stationId, route.travelTime / 60, route.distance);
                }

                appStatusBean.increment();
                //LOGGER.info("point processed");
            }
            exchange.getOut().setBody("{\"total\":" + geos.size() + ",\"current\": " + geos.size() + ",\"busy\":0}");
            appStatusBean.setBusy(false);


        } finally {

            if (rs != null && !rs.isClosed()) {
                rs.close();
            }
            if (statementSelect != null && !statementSelect.isClosed()) {
                statementSelect.close();
            }
            if (statementDelete != null && !statementDelete.isClosed()) {
                statementDelete.close();
            }
            if (statementInsert != null && !statementInsert.isClosed()) {
                statementInsert.close();
            }

            if (connection != null) {
                connection.close();
            }

        }
    }
}
