package ru.ege.geo.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.ege.geo.bean.AppStatusBean;
import ru.ege.geo.bean.MetroBean;
import ru.ege.geo.bean.MetroPathStorageBean;
import ru.ege.geo.bean.MetroStorageBean;
import ru.ege.geo.bean.ObstacleStorageBean;
import ru.ege.geo.dao.GeoDAO;
import ru.ege.geo.json.JSONRoute;
import ru.ege.geo.json.JSONRouteList;
import ru.ege.geo.json.JSONRouteMixin;
import ru.ege.geo.util.GeoPoint;
import ru.ege.geo.util.RouteBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author afilatov
 */
public class ReloadAllMetroStatusProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReloadAllMetroStatusProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {

        Map<String, Integer> response = new HashMap<>();

        AppStatusBean appStatusBean = AppStatusBean.getInstance();
        response.put("busy", appStatusBean.isBusy() ? 1 : 0);
        response.put("total", appStatusBean.getTotal());
        response.put("current", appStatusBean.getCurrent());

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, true);
        String json = mapper.writeValueAsString(response);
        exchange.getOut().setBody(json);
    }


}
