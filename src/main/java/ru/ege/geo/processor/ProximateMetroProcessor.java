package ru.ege.geo.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ege.geo.bean.MetroBean;
import ru.ege.geo.bean.MetroPathStorageBean;
import ru.ege.geo.bean.MetroStorageBean;
import ru.ege.geo.bean.ObstacleStorageBean;
import ru.ege.geo.json.JSONRoute;
import ru.ege.geo.json.JSONRouteList;
import ru.ege.geo.json.JSONRouteMixin;
import ru.ege.geo.util.GeoPoint;
import ru.ege.geo.util.RouteBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author afilatov
 */
public class ProximateMetroProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProximateMetroProcessor.class);

    public static final int MINUTES_25 = 1500;
    public static final double DISTANCE_SPEED_FACTOR = 1.2;

    private ObstacleStorageBean obstaclesBean;
    private MetroStorageBean metroBean;
    private MetroPathStorageBean metroPathBean;

    public ObstacleStorageBean getObstaclesBean() {
        return obstaclesBean;
    }

    public void setObstaclesBean(ObstacleStorageBean obstaclesBean) {
        this.obstaclesBean = obstaclesBean;
    }

    public MetroStorageBean getMetroBean() {
        return metroBean;
    }

    public void setMetroBean(MetroStorageBean metroBean) {
        this.metroBean = metroBean;
    }

    public MetroPathStorageBean getMetroPathBean() {
        return metroPathBean;
    }

    public void setMetroPathBean(MetroPathStorageBean metroPathBean) {
        this.metroPathBean = metroPathBean;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        HttpServletRequest request = exchange.getIn().getBody(HttpServletRequest.class);

        double latA = 0, lngA = 0;

        Enumeration enParams = request.getParameterNames();
        while (enParams.hasMoreElements()) {
            try {
                String paramName = (String) enParams.nextElement();
                switch (paramName) {
                    case "latA":
                        latA = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "lngA":
                        lngA = Double.parseDouble(request.getParameter(paramName));
                        break;
                }
            } catch (NumberFormatException e) {
                // ignored
            }
        }

        ExecutorService pool = Executors.newFixedThreadPool(10);
        TreeMap<Integer, JSONRoute> finalRoutes = buildRoutes(pool, latA, lngA, obstaclesBean, metroBean, metroPathBean);

        ObjectMapper mapper = new ObjectMapper();
        mapper.getSerializationConfig().addMixInAnnotations(JSONRoute.class, JSONRouteMixin.class);
        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, true);
        String json = mapper.writeValueAsString(finalRoutes.values());
        exchange.getOut().setBody(json);
    }

    public static TreeMap<Integer, JSONRoute> buildRoutes(ExecutorService pool,
                                                          double latA, double lngA,
                                                          ObstacleStorageBean obstaclesBean, MetroStorageBean metroBean, MetroPathStorageBean metroPathBean) throws InterruptedException, java.util.concurrent.ExecutionException {
        Set<Future<JSONRouteList>> futureResultSet = new HashSet<>();

        TreeMap<Double, MetroBean> directDistance = metroBean.getNearestStations(latA, lngA);
        int i = 0;
        while (i < 11) {
            Map.Entry<Double, MetroBean> entry = directDistance.pollFirstEntry();
            // Callable<JSONRouteList> task = new RouteBuilder(obstaclesBean, new GeoPoint(latA, lngA), new GeoPoint(0, 0));
            Callable<JSONRouteList> task = new RouteBuilder(RouteBuilder.Filter.SHORTEST_1, RouteBuilder.Spread.CHOP,
                    obstaclesBean, new GeoPoint(latA, lngA), entry.getValue());
            Future<JSONRouteList> futureResult = pool.submit(task);
            futureResultSet.add(futureResult);

            i++;
        }

        List<JSONRoute> bypassDistance = new LinkedList<>();
        for (Future<JSONRouteList> futureResult : futureResultSet) {
            JSONRouteList routeBuilderResult = futureResult.get();
            if (!routeBuilderResult.routes.isEmpty()) {
                // считаем время по приведённому маршруту
                bypassDistance.add(routeBuilderResult.routes.getFirst());
            }
        }
        Collections.sort(bypassDistance, new Comparator<JSONRoute>() {
            @Override
            public int compare(JSONRoute o1, JSONRoute o2) {
                return Double.valueOf(o1.distance).compareTo(o2.distance);
            }
        });

        Map<String, Integer> metroLines = new HashMap<>();
        TreeMap<Integer, JSONRoute> finalRoutes = new TreeMap<>();

        boolean hasPedestrianProximity = false;
        // итерация по сортированным !!! маршрутам
        for (JSONRoute route : bypassDistance) {
            if (route.distance > DISTANCE_SPEED_FACTOR && hasPedestrianProximity)
                continue;

            int travelTime = route.getTravelTime();
            route.travelTime = route.getTravelTime();

            boolean lineBeforeLimit = false;
            for (String line : route.metroStation.getLines()) {
                if (!metroLines.containsKey(line)) {
                    metroLines.put(line, metroLines.size() + 1);
                }
                if (metroLines.get(line) < 4) {
                    lineBeforeLimit = true;
                }
            }

            boolean graphSearch = false;

            for (JSONRoute routeTransfer : bypassDistance) {
                if (!routeTransfer.equals(route)) {
                    if (travelTime > routeTransfer.getTravelTime() + metroPathBean.getTravelTime(routeTransfer.metroStation, route.metroStation)) {
                        graphSearch = true;
                        break;
                    }
                }
            }

            if (travelTime < MINUTES_25) {
                if (lineBeforeLimit && !graphSearch) {
                    finalRoutes.put(travelTime, route);
                    if (route.distance <= DISTANCE_SPEED_FACTOR)
                        hasPedestrianProximity = true;
                }
            } else {
                if (finalRoutes.size() < 2 && (finalRoutes.isEmpty() || finalRoutes.lastKey() > MINUTES_25)) {
                    finalRoutes.put(travelTime, route);
                    if (route.distance <= DISTANCE_SPEED_FACTOR)
                        hasPedestrianProximity = true;
                }
            }
        }

        if (!hasPedestrianProximity && !finalRoutes.isEmpty()) {
            Integer nearestTransportRoute = finalRoutes.firstKey();
            Iterator<Map.Entry<Integer, JSONRoute>> iterator = finalRoutes.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<Integer, JSONRoute> entry = iterator.next();
                if (entry.getKey() > nearestTransportRoute * 3) {
                    iterator.remove();
                }
            }

        }
        return finalRoutes;
    }


}
