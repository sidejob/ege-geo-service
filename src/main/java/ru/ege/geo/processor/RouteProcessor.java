package ru.ege.geo.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ege.geo.bean.ObstacleStorageBean;
import ru.ege.geo.json.JSONRouteList;
import ru.ege.geo.util.GeoPoint;
import ru.ege.geo.util.RouteBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.concurrent.Callable;

/**
 * @author afilatov
 */
public class RouteProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(RouteProcessor.class);

    private ObstacleStorageBean obstaclesBean;

    public ObstacleStorageBean getObstaclesBean() {
        return obstaclesBean;
    }

    public void setObstaclesBean(ObstacleStorageBean obstaclesBean) {
        this.obstaclesBean = obstaclesBean;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        HttpServletRequest request = exchange.getIn().getBody(HttpServletRequest.class);
        HttpServletResponse response = (HttpServletResponse) exchange.getIn().getHeader(Exchange.HTTP_SERVLET_RESPONSE);
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        response.setContentType("text/javascript");


        /*{ // POST 1
            StringBuffer jb = new StringBuffer();
            String line = null;
            try {
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null)
                    jb.append(line);
                LOGGER.info("POST body - " + jb);
            } catch (Exception e) {
            }
        }
        { // POST 2
            StringBuilder stringBuilder = new StringBuilder(1000);
            Scanner scanner = new Scanner(request.getInputStream());
            while (scanner.hasNextLine()) {
                stringBuilder.append(scanner.nextLine());
            }

            String body = stringBuilder.toString();
            LOGGER.info("POST body - " + body);
        }    */


        double latA = 0, lngA = 0, latB = 0, lngB = 0, latC = 0, lngC = 0, latD = 0, lngD = 0;

        Enumeration enParams = request.getParameterNames();
        while (enParams.hasMoreElements()) {
            try {
                String paramName = (String) enParams.nextElement();
                switch (paramName) {
                    case "latA":
                        latA = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "lngA":
                        lngA = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "latB":
                        latB = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "lngB":
                        lngB = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "latC":
                        latC = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "lngC":
                        lngC = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "latD":
                        latD = Double.parseDouble(request.getParameter(paramName));
                        break;
                    case "lngD":
                        lngD = Double.parseDouble(request.getParameter(paramName));
                        break;
                }
            } catch (NumberFormatException e) {
            }
        }

        {
            // поиск пересечений kml и линии
//            List<GeoPoint> geoPoints = obstacleStorageBean.findIntersections(new GeoPoint(latA, lngA), new GeoPoint(latB, lngB));
//            exchange.getOut().setBody(Arrays.toString(geoPoints.toArray()));
        }

//        List<GeoPoint> geoPoints = obstacleStorageBean.findMinRoute(new GeoPoint(latA, lngA), new GeoPoint(latB, lngB));
//        exchange.getOut().setBody(Arrays.toString(geoPoints.toArray()));


        Callable<JSONRouteList> task = new RouteBuilder(RouteBuilder.Filter.SHORTEST_2, RouteBuilder.Spread.CHOP,
                obstaclesBean, new GeoPoint(latA, lngA), new GeoPoint(latB, lngB));
        JSONRouteList result = task.call();

//        RouteBuilder routeBuilder = new RouteBuilder(obstaclesBean, new GeoPoint(latA, lngA), new GeoPoint(latB, lngB));
//        RouteBuilderResult routeBuilderResult = routeBuilder.call();

//        String json = obstacleStorageBean.findAllRoutes(new GeoPoint(latA, lngA), new GeoPoint(latB, lngB));

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, true);

        String json = mapper.writeValueAsString(result.routes);
        // json = json.replaceAll("(?<!\\\\)\"", "'");

        exchange.getOut().setBody(json);

    }
}
