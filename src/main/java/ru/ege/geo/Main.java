package ru.ege.geo;

public final class Main {

    private Main() {
    }

    public static void main(String[] args) throws Exception {
        org.apache.camel.spring.Main main = new org.apache.camel.spring.Main();
        main.setApplicationContextUri("standalone.xml");
        main.enableHangupSupport();
        main.run();
    }
}
