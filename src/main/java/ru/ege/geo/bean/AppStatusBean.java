package ru.ege.geo.bean;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author afilatov
 */
public class AppStatusBean {
    private static AppStatusBean instance = new AppStatusBean();

    private AtomicBoolean busy = new AtomicBoolean(false);
    private AtomicInteger total = new AtomicInteger(0);
    private AtomicInteger current = new AtomicInteger(0);

    public Integer getTotal() {
        return total.get();
    }

    public Integer getCurrent() {
        return current.get();
    }

    public Boolean isBusy() {
        return busy.get();
    }

    public void setBusy(boolean busy) {
        this.busy.set(busy);
    }

    public void reset(int total) {
        this.total.set(total);;
        this.current.set(0);
    }

    public void increment() {
        current.incrementAndGet();
    }

    private AppStatusBean() {
    }

    public static AppStatusBean getInstance() {
        return instance;
    }

}
