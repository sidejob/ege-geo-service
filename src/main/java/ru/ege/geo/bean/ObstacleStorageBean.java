package ru.ege.geo.bean;

import com.bbn.openmap.geo.Geo;
import com.bbn.openmap.geo.Intersection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ru.ege.geo.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author afilatov
 */
public class ObstacleStorageBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(ObstacleStorageBean.class);

    private List<GeoObstacle> obstacles = Collections.emptyList();
    private String fileName;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void init() throws Exception {
        LOGGER.info(">> Start loading obstacles ...");
        obstacles = new ArrayList<>();

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(getFileName());

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(is);

        doc.getDocumentElement().normalize();
        NodeList placemarkList = doc.getElementsByTagName("Placemark");
        String coordinates = null;
        ObstacleType obstacleType = null;
        for (int temp = 0; temp < placemarkList.getLength(); temp++) {
            Node placemarkNode = placemarkList.item(temp);

            if (placemarkNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) placemarkNode;
                NodeList lineStrings = eElement.getElementsByTagName("LineString");
                if (lineStrings.getLength() > 0) {
                    obstacleType = ObstacleType.LINE;
                    coordinates = ((Element) lineStrings.item(0)).getElementsByTagName("coordinates").item(0).getTextContent();
                }

                NodeList linearRing = eElement.getElementsByTagName("LinearRing");
                if (linearRing.getLength() > 0) {
                    obstacleType = ObstacleType.RING;
                    coordinates = ((Element) linearRing.item(0)).getElementsByTagName("coordinates").item(0).getTextContent();
                }

                GeoObstacle geoObstacle = GeoObstacle.parseString(coordinates, obstacleType, obstacles.size());
                obstacles.add(geoObstacle);
            }
        }


        LOGGER.info(">> ObstacleStorageBean.size = " + obstacles.size());
    }

    public List<GeoPoint> findIntersections(GeoPoint from, GeoPoint to) {
        List<GeoPoint> intersections = new LinkedList<>();

        for (GeoObstacle geoObstacle : obstacles) {
            for (GeoObstacleSeg segment : geoObstacle.getSegments()) {
                double[] inter = Intersection.getSegIntersection(
                        from.getLat(), from.getLng(),
                        to.getLat(), to.getLng(),
                        segment.from.getLat(), segment.from.getLng(),
                        segment.to.getLat(), segment.to.getLng());

                if (inter[0] != Double.MAX_VALUE) {
                    intersections.add(new GeoPoint(inter[0], inter[1]));
                }
                if (inter[2] != Double.MAX_VALUE) {
                    intersections.add(new GeoPoint(inter[2], inter[3]));
                }
            }
        }
        return intersections;
    }

    public boolean hasIntersectionsWithSelfExeptions(GeoPoint from, GeoPoint to) {
        for (GeoObstacle geoObstacle : obstacles) {
            for (GeoObstacleSeg segment : geoObstacle.getSegments()) {
                if (segment.containsPoint(from) || segment.containsPoint(to)) {
                    continue;
                }
                double[] inter = Intersection.getSegIntersection(
                        from.getLat(), from.getLng(),
                        to.getLat(), to.getLng(),
                        segment.from.getLat(), segment.from.getLng(),
                        segment.to.getLat(), segment.to.getLng());

                if (inter[0] != Double.MAX_VALUE || inter[2] != Double.MAX_VALUE) {
                    return true;
                }
            }
        }
        return false;
    }

    public GeoCrossing findClosestIntersection(GeoPoint from, GeoPoint to) {
        GeoCrossing result = null;
        Geo geoFrom = new Geo(from.getLat(), from.getLng());
        double distance = Double.MAX_VALUE;

        for (GeoObstacle obstacle : obstacles) {
            for (GeoObstacleSeg segment : obstacle.getSegments()) {
                if (segment.containsPoint(from) || segment.containsPoint(to)) {
                    continue;
                }

                double[] inter = Intersection.getSegIntersection(
                        from.getLat(), from.getLng(),
                        to.getLat(), to.getLng(),
                        segment.from.getLat(), segment.from.getLng(),
                        segment.to.getLat(), segment.to.getLng());

                GeoPoint geoPoint = null;
                if (inter[0] != Double.MAX_VALUE) {
                    geoPoint = new GeoPoint(inter[0], inter[1]);
                }
                if (inter[2] != Double.MAX_VALUE) {
                    geoPoint = new GeoPoint(inter[2], inter[3]);
                }

                if (geoPoint != null) {
                    Geo geoTo = new Geo(geoPoint.getLat(), geoPoint.getLng());
                    double tmp = geoFrom.distanceKM(geoTo);
                    if (tmp < distance) {
                        result = new GeoCrossing(obstacle, segment, geoPoint);
                        distance = tmp;
                    }
                }
            }
        }
        return result;
    }


    public boolean isPointInObstacle(GeoPoint point) {
        Geo geoPoint = new Geo(point.getLat(), point.getLng());
        for (GeoObstacle obstacle : obstacles) {
            if (ObstacleType.RING.equals(obstacle.getType())) {

                double[] poly = new double[obstacle.getPoints().size() * 2 + 2];
                for (int i = 0; i < obstacle.getPoints().size(); i++) {
                    poly[i * 2] = obstacle.getPoints().get(i).getLat();
                    poly[i * 2 + 1] = obstacle.getPoints().get(i).getLng();
                }
                poly[obstacle.getPoints().size() * 2] = obstacle.getPoints().get(0).getLat();
                poly[obstacle.getPoints().size() * 2 + 1] = obstacle.getPoints().get(0).getLng();

                if(Intersection.isPointInPolygon(geoPoint, poly, true))
                    return true;
            }
        }
        return false;
    }

}

