package ru.ege.geo.bean;

import com.bbn.openmap.geo.Geo;
import org.apache.camel.Exchange;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

/**
 * @author afilatov
 */
public class MetroStorageBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(MetroStorageBean.class);

    private List<MetroBean> stations = Collections.emptyList();

    public void init(Exchange exchange) throws Exception {
        LOGGER.info(">> Start loading metro stations ...");
        List list = (ArrayList) exchange.getIn().getBody();
        stations = new ArrayList<>(list.size());

        for (Object record : list) {
            MetroBean metroBean = new MetroBean();
            LinkedCaseInsensitiveMap map = (LinkedCaseInsensitiveMap) record;
            for (Object key : map.keySet()) {
                // LOGGER.info("key = " + key + ", value = " + map.get(key));
                switch ((String) key) {
                    case "id":
                        metroBean.setId((Integer) map.get(key));
                        break;
                    case "title":
                        metroBean.setTitle((String) map.get(key));
                        break;
                    case "lat":
                        metroBean.setLat(((Float) map.get(key)).doubleValue());
                        break;
                    case "lng":
                        metroBean.setLng(((Float) map.get(key)).doubleValue());
                        break;
                    case "lines":
                        for (String line : StringUtils.split((String) map.get(key), ",")) {
                            metroBean.getLines().add(line);
                        }
                        break;
                    default:
                        break;
                }
            }
            stations.add(metroBean);
        }
        LOGGER.info(">> MetroStorageBean.size = " + stations.size());
    }

    @SuppressWarnings("unused")
    public List<MetroBean> getStations() {
        return stations;
    }

    public TreeMap<Double, MetroBean> getNearestStations(double latA, double lngA) {
        TreeMap<Double, MetroBean> result = new TreeMap<>();
        Geo point1 = new Geo(latA, lngA);
        for (MetroBean metroBean : stations) {
            Geo point2 = new Geo(metroBean.getLat(), metroBean.getLng());
            result.put(point1.distanceKM(point2), metroBean);
        }
        return result;
    }
}
