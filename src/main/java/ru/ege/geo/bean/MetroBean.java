package ru.ege.geo.bean;

import java.util.HashSet;
import java.util.Set;

/**
 * @author afilatov
 */
public class MetroBean {


    private Integer id;
    private String title;
    private double lat;
    private double lng;
    private Set<String> lines = new HashSet<>();

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLat() {
        return lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLng() {
        return lng;
    }

    public Set<String> getLines() {
        return lines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MetroBean metroBean = (MetroBean) o;

        if (id != null ? !id.equals(metroBean.id) : metroBean.id != null) return false;
        if (title != null ? !title.equals(metroBean.title) : metroBean.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
