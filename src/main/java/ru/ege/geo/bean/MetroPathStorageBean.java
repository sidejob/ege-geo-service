package ru.ege.geo.bean;

import org.apache.camel.Exchange;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author afilatov
 */
public class MetroPathStorageBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(MetroPathStorageBean.class);

    private Map<String, Long> pathWays = Collections.emptyMap();

    public void init(Exchange exchange) throws Exception {
        LOGGER.info(">> Start loading metro pathways ...");
        List list = (ArrayList) exchange.getIn().getBody();
        pathWays = new HashMap<>();

        for (Object record : list) {
            LinkedCaseInsensitiveMap map = (LinkedCaseInsensitiveMap) record;
            BigInteger minPlaceId = null, maxPlaceId = null;
            Long distance = null;

            for (Object key : map.keySet()) {
                // LOGGER.info("key = " + key + ", value = " + map.get(key));
                switch ((String) key) {
                    case "min_place_id":
                        minPlaceId = (BigInteger) map.get(key);
                        break;
                    case "max_place_id":
                        maxPlaceId = (BigInteger) map.get(key);
                        break;
                    case "distance":
                        distance = (Long) map.get(key);
                        break;
                    default:
                        break;
                }
            }
            if(minPlaceId != null && maxPlaceId != null && distance != null) {
                pathWays.put(String.valueOf(minPlaceId) + "," + String.valueOf(maxPlaceId), distance);
            }
        }
        LOGGER.info(">> MetroPathStorageBean.size = " + pathWays.size());
    }

    public Integer getTravelTime(MetroBean station1, MetroBean station2) {
        Long mins = pathWays.get(Math.min(station1.getId(), station2.getId()) + "," + Math.max(station1.getId(), station2.getId()));
        if(mins == null) {
            return Integer.MAX_VALUE;
        } else {
            return mins.intValue() * 60;
        }
    }
}
