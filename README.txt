EGE-GEO-SERVICE
===============

Веб сервис, принимает запросы вида:

http://127.0.0.1:8282/ege/geo/routes?latA=12.123&lngA=34.45&latB=13.123&lngB=35.45

где (latA, lngA) координаты первой точки, (latB, lngB) соответственно второй, для нахождения
оптимального маршрута на карте в обход массива препятствий, описанных в файле

\src\main\bundle\conf\m7VkvrCNXVU4JzIqoX6RMxslbpB9i54B.kml.xml